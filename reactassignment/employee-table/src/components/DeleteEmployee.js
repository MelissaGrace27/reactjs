import React, { Component } from 'react'
import { Modal, Button } from 'react-bootstrap'
import 'bootstrap/dist/css/bootstrap.min.css';

export class DeleteEmployee extends Component {
    constructor(props) {
        super(props);
        this.state = {
            empid: '',
            data: '',
            deleteModalShow: this.props.show
        };
        this.deleteEmployee = this.deleteEmployee.bind(this);
        this.handleClose = this.handleClose.bind(this);
    }
    deleteEmployee(empid) {
        let items = JSON.parse(localStorage.getItem("data"));
        console.log('items is:', items);
        items = items.filter((data) => data['Emp ID'] !== empid);
        let data = localStorage.setItem("data", JSON.stringify(items));
        console.log('emp id: ', empid);
        if (items.length === 0) {
            localStorage.removeItem("data");
        }
        alert('The record has been deleted');
    }
    handleClose() {
        console.log("second function")
        this.setState({
            deleteModalShow: false
        })
    }
    render() {
        const { onHide, show } = this.state;
        return (
            <div>
                <Modal {...this.props} aria-labelledby="contained-modal-title-vcenter" centered>
                    <Modal.Header closeButton>
                        <Modal.Title id="contained-modal-title-vcenter">Employee Details</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <div className="container">
                            <p>Are you sure you want to delete this Record?</p>
                            <p className="text-warning"><small>This action cannot be undone.</small></p>
                        </div>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="primary" onClick={this.props.onHide}>Cancel</Button>
                        <Button variant="danger" type="submit" onClick={() => {
                            this.deleteEmployee(this.props.empid)
                            this.handleClose()
                        }}>Yes, Delete</Button>
                    </Modal.Footer>
                </Modal>
            </div>
        )
    }
}

export default DeleteEmployee;

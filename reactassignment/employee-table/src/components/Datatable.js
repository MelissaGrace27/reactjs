import { DataTable } from 'primereact/datatable';
import { Component } from 'react';
import '../DataTableDemo.css';

class Datatable extends Component {
    constructor(props) {
        super(props);
        this.state = {
            tableValue: []
        };
    }
    render() {
        return (
            <div>
                <DataTable className='table-wrapper shadow table-sm p-datatable-lg mb-5'
                    value={this.props.tableValue} resizableColumns paginator rows={10} >
                    {this.props.children}
                </DataTable>
            </div>
        )
    }
}
export default Datatable;